import React from 'react'
import { mount } from 'enzyme'
import Header from '../Header'

describe('<Header />', () => {
	it('should match snapshot', () => {
		const defaultProps = {
			username: 'matt',
			logoutUser: jest.fn()
		}
		const wrapper = mount(<Header {...defaultProps } />)
		expect(wrapper).toMatchSnapshot()
	})

	it('should able to logout user', () => {
		const defaultProps = {
			username: 'Tim',
			logoutUser: jest.fn()
		}

		const wrapper = mount(<Header {...defaultProps } />)
		wrapper.find('button[data-test="logout-user"]').simulate('click')
		expect(defaultProps.logoutUser).toBeCalled()
	})
})
