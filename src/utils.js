const users = [
	{ id: '1', name: 'alex40', password: 'test' },
	{ id: '2', name: 'tom08', password: 'test' },
	{ id: '3', name: 'jerry01', password: 'test' }
]

export const unSetUser = () => localStorage.removeItem('user')

export const getUser = () => JSON.parse(localStorage.getItem('user'))

export const setUser = (user) => localStorage.setItem('user', JSON.stringify(user))

export const getTodoList = (user_id) => JSON.parse(localStorage.getItem(`todo_${user_id}`))

export const setTodoList = (user_id, TodoList) => localStorage.setItem(`todo_${user_id}`, JSON.stringify(TodoList))

export const refreshStorage = () =>
	Object.keys(localStorage)
		.filter((item) => item.startsWith('todo_'))
		.forEach((item) => localStorage.removeItem(item))

export const allowedUsers = () => JSON.parse(localStorage.getItem('allowed_users'))

export const usersListToStorage = () => localStorage.setItem('allowed_users', JSON.stringify(users))
