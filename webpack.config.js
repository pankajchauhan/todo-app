const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
	devtool: "source-map",
	mode: "development",
	target: "web",
	entry: "./src",
	output: {
		publicPath: "/",
		filename: "bundle.js",
		path: path.resolve(__dirname, "dist"),
	},
	module: {
		rules: [{
			test: /\.css$/,
			use: [
				'style-loader',
				'css-loader',
			],
		}, {
			test: /\.jsx?$/,
			exclude: /(node_modules|bower_components)/,
			loader: "babel-loader?cacheDirectory",
		}],
	},
	devServer: {
		hot: true,
		port: 3000,
		stats: 'errors-only',
		contentBase: 'public',
		historyApiFallback: {
			index: '/'
		},
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: path.resolve('public', 'index.html')
		}),
		new webpack.HotModuleReplacementPlugin()
	]
}