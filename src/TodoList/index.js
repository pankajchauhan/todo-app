import React, { Component, Fragment } from 'react'
import { Container, Jumbotron } from 'reactstrap'
import { unSetUser, getUser, getTodoList, setTodoList } from '../utils'
import CreateTodo from './CreateTodo'
import TodoItem from './TodoItem'
import Header from './Header'

class TodoList extends Component {
	constructor(props) {
		super(props)
		const { id, name } = getUser()
		this.state = { username: name, user_id: id, EditTodo: {}, TodoList: getTodoList(id) || [] }
	}

	logout = () => {
		unSetUser()
		this.props.history.push('/login')
	}

	editTodo = (EditTodo) => this.setState({ EditTodo })

	updateTodoList(TodoList) {
		const { user_id } = this.state
		this.setState({ TodoList })
		setTodoList(user_id, TodoList)
	}

	addTodo = (TodoItem) => {
		const { TodoList } = this.state
		TodoList.push(TodoItem)
		this.updateTodoList(TodoList)
	}

	updateTodo = (TodoItem) => {
		const { id } = TodoItem
		const { TodoList } = this.state
		const ITEM_INDEX = TodoList.findIndex((x) => x.id === id)

		TodoList[ITEM_INDEX] = TodoItem
		this.setState({ EditTodo: {} })
		this.updateTodoList(TodoList)
	}

	deleteTodo = (todo_id) => {
		const { TodoList } = this.state
		const filteredList = TodoList.filter((x) => x.id !== todo_id)

		this.updateTodoList(filteredList)
	}

	render() {
		const { username, TodoList, EditTodo } = this.state
		const { addTodo, editTodo, updateTodo, deleteTodo } = this
		return (
			<Fragment>
				<Header logoutUser={this.logout} username={username} />
				<Container className='mt-5'>
					<CreateTodo {...{ addTodo, updateTodo, EditTodo }} />
					<h5 className='mt-5'>Tasks in Hand</h5>
					<hr className='mt-2' />
					{TodoList.map((todo) => (
						<TodoItem key={todo.id} {...{ editTodo, deleteTodo, updateTodo, ...todo }} />
					))}
					{!TodoList.length && (
						<Jumbotron className='text-center'>
							<h5>No Tasks Added yet</h5>
							Create one via form above
						</Jumbotron>
					)}
				</Container>
			</Fragment>
		)
	}
}

export default TodoList
