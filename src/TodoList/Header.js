import React from 'react'
import { Navbar, Button, Container } from 'reactstrap'

const Header = ({ username, logoutUser }) => (
	<Navbar color='light'>
		<Container>
			<span>
				Welcome <strong>{username.toUpperCase()}!</strong>
			</span>
			<Button data-test='logout-user' onClick={logoutUser}>
				Logout
			</Button>
		</Container>
	</Navbar>
)

export default Header
