import React from 'react'
import { render } from 'react-dom'
import 'bootstrap/dist/css/bootstrap.css'
import { refreshStorage, usersListToStorage } from './utils'
import App from './App'

refreshStorage()
usersListToStorage()

render(<App />, document.getElementById('main-app'))
