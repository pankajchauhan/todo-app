import { setUser, allowedUsers } from '../utils'

export const isAuthorized = ({ username, password }) => {
	const user = allowedUsers().find((user) => user.name === username && user.password === password)
	if (user) {
		const { id, name } = user
		setUser({ id, name })
		return true
	}
	return false
}
