import React from 'react'
import { hot } from 'react-hot-loader'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faPen, faTrash } from '@fortawesome/free-solid-svg-icons'
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom'
import LoginPage from './LoginPage'
import TodoList from './TodoList'
import { getUser } from './utils'

library.add(faPen, faTrash)

const App = () => (
	<BrowserRouter>
		<Switch>
			<Route path='/login' component={LoginPage} />
			<Route path='/' render={(props) => (getUser() ? <TodoList {...props} /> : <Redirect to='/login' />)} />
		</Switch>
	</BrowserRouter>
)

export default hot(module)(App)
