import React, { Component } from 'react'
import { Container, Row, Col, Button, Form, FormGroup, Label, Input, Alert } from 'reactstrap'
import { isAuthorized } from './auth'
import './style.css'

class LoginPage extends Component {
	constructor(props) {
		super(props)
		this.state = { error: '', username: '', password: '' }
	}

	onInputChange = ({ target: { name, value } }) => this.setState({ [name]: value })

	formSubmit = (e) => {
		e.preventDefault()
		const { history } = this.props
		const { username, password } = this.state
		const authorized = isAuthorized({ username: username.toLowerCase(), password })

		if (authorized) return history.push('/')
		else return this.setState({ error: 'Incorrect username or password' })
	}

	render() {
		const { error, username, password } = this.state
		return (
			<Container>
				<Row className='justify-content-center'>
					<Col sm='auto'>
						<div className='text-center mt-5'>
							Todo <strong>List</strong>
						</div>
						<Form className='login-form p-4' onSubmit={this.formSubmit}>
							{error && (
								<Alert color='danger' data-test='alert-error' className='text-center'>
									{error}
								</Alert>
							)}
							<FormGroup>
								<Label for='username'>Username</Label>
								<Input
									required
									type='text'
									id='username'
									name='username'
									value={username}
									placeholder='Username...'
									onChange={this.onInputChange}
								/>
							</FormGroup>
							<FormGroup>
								<Label for='password'>Password</Label>
								<Input
									required
									id='password'
									type='password'
									name='password'
									value={password}
									placeholder='Password...'
									onChange={this.onInputChange}
								/>
							</FormGroup>
							<Button className='my-4' color='primary' type='submit' block>
								Login
							</Button>
						</Form>
					</Col>
				</Row>
			</Container>
		)
	}
}

export default LoginPage
