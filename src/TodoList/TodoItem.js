import React, { Component } from 'react'
import { Row, Col, Button, CustomInput, Collapse, CardBody, Card } from 'reactstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import PropTypes from 'prop-types'
import moment from 'moment'

class TodoItem extends Component {
	constructor(props) {
		super(props)
		this.state = { collapse: false }
	}

	static propTypes = {
		id: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]).isRequired,
		name: PropTypes.string.isRequired,
		isComplete: PropTypes.bool.isRequired,
		description: PropTypes.string.isRequired,
		editTodo: PropTypes.func.isRequired,
		deleteTodo: PropTypes.func.isRequired,
		updateTodo: PropTypes.func.isRequired
	}

	toggle = () => this.setState({ collapse: !this.state.collapse })

	render() {
		const { editTodo, deleteTodo, updateTodo, ...todoItem } = this.props
		const { id, name, isComplete, description } = todoItem
		return (
			<Row className='py-1 align-items-center'>
				<Col sm='auto'>
					<CustomInput
						id={id}
						type='checkbox'
						checked={isComplete}
						onChange={() => updateTodo({ ...todoItem, isComplete: !isComplete })}
					/>
				</Col>
				<Col>
					<Button
						color='link'
						onClick={this.toggle}
						style={{ textDecoration: isComplete ? 'line-through' : undefined }}>
						{name}
					</Button>
				</Col>
				<Col sm='auto'>
					<Button onClick={() => editTodo(todoItem)} color='link'>
						<FontAwesomeIcon icon='pen' />
					</Button>
				</Col>
				<Col sm='auto'>
					<Button onClick={() => deleteTodo(id)} color='link'>
						<FontAwesomeIcon icon='trash' />
					</Button>
				</Col>
				<Col xs='12'>
					<Collapse isOpen={this.state.collapse}>
						<Card>
							<CardBody>
								<Row className='mb-3'>
									<Col>
										<small className='text-muted'>
											{moment(id).format('MMMM Do YYYY, h:mm:ss a')}
										</small>
									</Col>
									<Col xs='auto'>
										<small className='text-muted'>{moment(id).fromNow()}</small>
									</Col>
								</Row>
								<dl>
									<dt>{name}</dt>
									<dd>{description}</dd>
								</dl>
							</CardBody>
						</Card>
					</Collapse>
				</Col>
			</Row>
		)
	}
}

export default TodoItem
