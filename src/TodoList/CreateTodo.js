import React, { Fragment, Component } from 'react'
import { Col, Form, FormGroup, Label, Input, Button } from 'reactstrap'

class CreateTodo extends Component {
	constructor(props) {
		super(props)
		this.state = { name: '', description: '' }
	}

	componentWillReceiveProps(nextProps) {
		const { name, description } = nextProps.EditTodo
		if (name && name !== this.props.EditTodo.name) {
			this.setState({ name, description })
		}
	}

	submitForm = (e) => {
		e.preventDefault()
		const { name, description } = this.state
		const { addTodo, updateTodo, EditTodo: { id } } = this.props
		const Todo = { id: Date.now(), name, description, isComplete: false }

		id ? updateTodo({ ...Todo, id }) : addTodo(Todo)
		this.setState({ name: '', description: '' })
	}

	render() {
		const { EditTodo } = this.props
		const { name, description } = this.state
		const type = EditTodo.id ? 'Update' : 'Add'
		return (
			<Fragment>
				<h5 className='mt-3'>{type} Todo</h5>
				<hr className='mt-2' />
				<Form className='my-4' onSubmit={this.submitForm}>
					<FormGroup row>
						<Label className='text-bold' for='name' sm={2}>
							Task Name
						</Label>
						<Col sm={10}>
							<Input
								type='text'
								name='name'
								id='name'
								value={name}
								placeholder='call Rick @7:00pm...'
								onChange={(e) => this.setState({ name: e.target.value })}
							/>
						</Col>
					</FormGroup>
					<FormGroup row>
						<Label for='Description' sm={2}>
							Description
						</Label>
						<Col sm={10}>
							<Input
								type='textarea'
								id='description'
								name='description'
								value={description}
								placeholder='Rick had unhandle mess, need to...'
								onChange={(e) => this.setState({ description: e.target.value })}
							/>
						</Col>
					</FormGroup>
					<FormGroup row>
						<Label sm={2} />
						<Col sm={10}>
							<Button color='primary' type='submit' block>
								{type} Todo
							</Button>
						</Col>
					</FormGroup>
				</Form>
			</Fragment>
		)
	}
}

export default CreateTodo
